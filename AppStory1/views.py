# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
def def_story1home(request) :
    return render(request, 'STORY1/home.html')