from django.db import models

# Create your models here.
class MataKuliah(models.Model) :
    nama_matkul = models.CharField('Nama Mata Kuliah:', help_text='Mata kuliah apa?', max_length=50, null=True)
    nama_dosen = models.CharField('Nama Dosen:', help_text='Siapa nama dosennya?', max_length=50, null=True)
    desc_matkul = models.CharField('Deskripsi Mata Kuliah:', help_text='Mata kuliah tentang apa?', max_length=250, null=True)
    tahun_matkul = models.CharField('Semester Tahun:', help_text='Semester berapa?', max_length=50, null=True)
    kelas_matkul = models.CharField('Ruang Kelas:', help_text='Di ruang mana?', max_length=50, null=True)
    jumlah_sks = models.IntegerField('Jumlah SKS:', help_text='Berapa SKS?', null=True)

    class Meta :
        db_table = "jadwalkuliah"