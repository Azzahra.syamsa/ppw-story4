"""PPW4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.def_home, name='def_home'),
    path('contact', views.def_contact, name='def_contact'),
    path('playlist', views.def_playlist, name='def_playlist'),
    path('portfolio', views.def_portfolio, name='def_portfolio'),
    path('form', views.def_form, name='def_form'),
    path('schedule', views.def_sched, name='def_sched'),
    path('detail/<int:id>/', views.def_detail, name='def_detail'),
]
