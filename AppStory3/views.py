from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import FormJadwal
from .models import MataKuliah

# Create your views here.
#Fungsi untuk contact.html, home.html, playlist.html, portfolio.html
def def_home(request):
    return render(request, 'home.html')
    
def def_contact(request):
    return render(request, 'contact.html')

def def_playlist(request):
    return render(request, 'playlist.html')

def def_portfolio(request):
    return render(request, 'portfolio.html')

#Fungsi untuk form.html
def def_form(request):
    #Untuk tempat query set
    listMatkul = FormJadwal(request.POST or None)

    #Untuk menambahkan mata kuliah
    if (listMatkul.is_valid() and request.method == 'POST'):
        listMatkul.save()
        return HttpResponseRedirect('/schedule')

    return render(request, 'form.html', {'hasil_form' : listMatkul})

#Fungsi untuk schedule.html
def def_sched(request) :
    listMatkul = MataKuliah.objects.all()
    return render(request, 'schedule.html', {'list_matkul' : listMatkul})

#Fungsi untuk detail.html
def def_detail(request, id) :
    if (request.method == 'POST'):
        MataKuliah.objects.get(id = request.POST['id']).delete()
        listMatkul = MataKuliah.objects.all()
        return render(request, 'schedule.html', {'list_matkul' : listMatkul})

    listMatkul = MataKuliah.objects.get(pk = id)
    return render(request, 'detail.html', {'list_matkul' : listMatkul})