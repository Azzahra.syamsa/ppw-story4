from django.test import TestCase, Client
from .views import def_home, def_contact, def_detail, def_form, def_playlist, def_portfolio, def_sched
from django.urls import resolve, reverse

# Create your tests here.
class TestApp3(TestCase):

    def test_url_home(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_contact(self):
        response = Client().get('/contact')
        self.assertEqual(response.status_code, 200)

    def test_url_playlist(self):
        response = Client().get('/playlist')
        self.assertEqual(response.status_code, 200)

    def test_url_portfolio(self):
        response = Client().get('/portfolio')
        self.assertEqual(response.status_code, 200)

    def test_url_form(self):
        response = Client().get('/form')
        self.assertEqual(response.status_code, 200)

    def test_url_schedule(self):
        response = Client().get('/schedule')
        self.assertEqual(response.status_code, 200)
    
    def test_url_detail(self):
        response = Client().get('/detail/<int:id>/')
        self.assertEqual(response.status_code, 200)